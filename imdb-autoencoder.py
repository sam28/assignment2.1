#!/bin/python3
import csv, random, argparse
import torch
import numpy as np
import pandas as pd
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset,DataLoader
from torch.utils.data import random_split
from gensim.models import KeyedVectors
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.sampler import SequentialSampler
import string

#---------------------------------------------------
class imdb_dataset(Dataset):
    def __init__(self,path,h,w,embad_model):
        self.data_frame=pd.read_csv(path,)
        self.model=embad_model
        self.h=h
        self.w=w
        self.table = str.maketrans('', '', string.punctuation)
        self.vocab,self.not_invocab,self.word_count,self.total_word=self.get_vacabulary()
        if 'zz' in self.word_count:
            self.word_count['zz']+=len(self)*self.h-self.total_word
        else:
            self.word_count['zz']=len(self)*self.h-self.total_word
        self.total_word=len(self)*self.h
        
        self.vocabmat=torch.rand(self.w,len(self.vocab),device=device)
        self.indtoword={}
        self.wordtoind={}
        for i,w in enumerate(self.vocab):
            self.vocabmat[:,i]=torch.tensor(self.model[w]/np.linalg.norm(self.model[w]),device=device)
            self.indtoword[i]=w
            self.wordtoind[w]=i
    
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        mat,label,word_weight,word_weight2=self.sen_to_mat(idx)
        return {'review':mat,'rating':label,"word_weight":word_weight,"word_weight2":word_weight2}
    
    def get_vacabulary(self):
        vocab=set()
        not_invocab=set()
        word_count={}
        word_count['za']=0
        total_word=0
        for i in range(len(self)):
            for j,w in enumerate((self.data_frame.loc[i][0]).split(' ')):
                w1=str.lower(w.translate(self.table))
                if w1 in self.model.vocab:
                    vocab.add(w1)
                    if w1 in word_count:
                        word_count[w1]+=1
                    else:
                        word_count[w1]=1
                else:
                    not_invocab.add(w1)
                    word_count['za']+=1
                total_word+=1
                if(j>=self.h-1):
                    break
        return vocab,not_invocab,word_count,total_word
    
    def sen_to_mat(self,idx):
        X=torch.zeros((self.h,self.w))
        word_weight=torch.ones(self.h)
        word_weight2=torch.ones(self.h)
        label=0
        for j,w in enumerate((self.data_frame.loc[idx][0]).split(' ')):
            w1=w.translate(self.table)
            w1=str.lower(w1)
            if w1 in self.model.vocab:
                b=self.model[w1]/np.linalg.norm(self.model[w1])
                X[j,:]=torch.Tensor(b).view(1,-1)
                word_weight[j]=1.0/self.word_count[w1]
                word_weight2[j]=np.exp(self.word_count[w1]*100/self.total_word)
            else:
                b=self.model['za']/np.linalg.norm(self.model['z'])
                X[j,:]=torch.Tensor(b).view(1,-1)
                word_weight[j]=1.0/self.word_count['za']
                word_weight2[j]=np.exp(self.word_count['za']*100/self.total_word)
            if(j>=self.h-1):
                break
        k=j
        b=self.model['zz']/np.linalg.norm(self.model['zz'])
        while(k<self.h):
            X[k,:]=torch.Tensor(b).view(1,-1)
            word_weight[j]=1.0/self.word_count['zz']
            word_weight2[j]=np.exp(self.word_count['zz']*100/self.total_word)
            k+=1

        if self.data_frame.loc[idx][1]=='pos':
            label=1
        else:
            label=0
        return X,label,word_weight,word_weight2
    
    
class imdb_dataset_test(Dataset):
    def __init__(self,path,h,w,embad_model):
        self.data_frame=pd.read_csv(path,)
        self.model=embad_model
        self.h=h
        self.w=w
        self.table = str.maketrans('', '', string.punctuation)
        self.vocab,self.not_invocab,self.word_count,self.total_word=self.get_vacabulary()
        if 'zz' in self.word_count:
            self.word_count['zz']+=len(self)*self.h-self.total_word
        else:
            self.word_count['zz']=len(self)*self.h-self.total_word
        self.total_word=len(self)*self.h
        
        self.vocabmat=torch.rand(self.w,len(self.vocab),device=device)
        self.indtoword={}
        self.wordtoind={}
        for i,w in enumerate(self.vocab):
            self.vocabmat[:,i]=torch.tensor(self.model[w]/np.linalg.norm(self.model[w]),device=device)
            self.indtoword[i]=w
            self.wordtoind[w]=i
    
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        mat,word_weight,word_weight2=self.sen_to_mat(idx)
        return {'review':mat,"word_weight":word_weight,"word_weight2":word_weight2}
    
    def get_vacabulary(self):
        vocab=set()
        not_invocab=set()
        word_count={}
        word_count['za']=0
        total_word=0
        for i in range(len(self)):
            for j,w in enumerate((self.data_frame.loc[i][0]).split(' ')):
                w1=str.lower(w.translate(self.table))
                if w1 in self.model.vocab:
                    vocab.add(w1)
                    if w1 in word_count:
                        word_count[w1]+=1
                    else:
                        word_count[w1]=1
                else:
                    not_invocab.add(w1)
                    word_count['za']+=1
                total_word+=1
                if(j>=self.h-1):
                    break
        return vocab,not_invocab,word_count,total_word
    
    def sen_to_mat(self,idx):
        X=torch.zeros((self.h,self.w))
        word_weight=torch.ones(self.h)
        word_weight2=torch.ones(self.h)
        label=0
        for j,w in enumerate((self.data_frame.loc[idx][0]).split(' ')):
            w1=w.translate(self.table)
            w1=str.lower(w1)
            if w1 in self.model.vocab:
                b=self.model[w1]/np.linalg.norm(self.model[w1])
                X[j,:]=torch.Tensor(b).view(1,-1)
                word_weight[j]=1.0/self.word_count[w1]
                word_weight2[j]=np.exp(self.word_count[w1]*100/self.total_word)
            else:
                b=self.model['za']/np.linalg.norm(self.model['z'])
                X[j,:]=torch.Tensor(b).view(1,-1)
                word_weight[j]=1.0/self.word_count['za']
                word_weight2[j]=np.exp(self.word_count['za']*100/self.total_word)
            if(j>=self.h-1):
                break
        k=j
        b=self.model['zz']/np.linalg.norm(self.model['zz'])
        while(k<self.h):
            X[k,:]=torch.Tensor(b).view(1,-1)
            word_weight[j]=1.0/self.word_count['zz']
            word_weight2[j]=np.exp(self.word_count['zz']*100/self.total_word)
            k+=1

#         if self.data_frame.loc[idx][1]=='pos':
#             label=1
#         else:
#             label=0
        return X,word_weight,word_weight2
    
class ADCNN(torch.nn.Module):
    def __init__(self,in_c,out_c,doc_len,kernel_height,kernel_width):
        super(ADCNN,self).__init__()
        self.strd=int(kernel_height/2)
        self.h1=int((doc_len-kernel_height)/self.strd)+1
        self.h2=int((self.h1-kernel_height)/self.strd)+1
        self.h3=int((self.h2-kernel_height)/self.strd)+1
        self.conv1=nn.Conv2d(in_c,out_c,(kernel_height,kernel_width),stride=self.strd)
        self.conv2=nn.Conv2d(out_c,2*out_c,(kernel_height,1),stride=self.strd)
        self.conv2E=nn.Conv2d(2*out_c,2*out_c,(kernel_height,1),stride=self.strd)
        self.conv3=nn.Conv2d(2*out_c,2*out_c,(self.h3,1),stride=self.strd)
        self.dconv3=nn.ConvTranspose2d(2*out_c,2*out_c,(self.h3,1),stride=self.strd)
        self.dconv2E=nn.ConvTranspose2d(2*out_c,2*out_c,(kernel_height,1),stride=self.strd)
        self.dconv2=nn.ConvTranspose2d(2*out_c,out_c,(kernel_height,1),stride=self.strd)
        self.dconv1=nn.ConvTranspose2d(out_c,in_c,(kernel_height,kernel_width),stride=self.strd)
        self.fc1=nn.Linear(2*out_c,20)
        self.fc2=nn.Linear(20,1)
    def forward(self,x):
        x1=F.relu(self.conv1(x))
        x2=F.relu(self.conv2(x1))
        x2e=F.relu(self.conv2E(x2))
        x3=F.relu(self.conv3(x2e))
        x4=F.relu(self.dconv3(x3))
        x5=F.relu(self.dconv2E(x4,output_size=x2.size()))
        x6=F.relu(self.dconv2(x5,output_size=x1.size()))
        x7=self.dconv1(x6, output_size=x.size())
        f1=self.fc1(x3.view(x3.shape[0],-1))
        f2=self.fc2(F.dropout(torch.sigmoid_(f1),p=0.5))
        f2=torch.sigmoid_(F.dropout(f2,p=.3))
        return x7,x3,f2

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("in main")
    args = parser.parse_args()
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    filename='glove.6B.50d.txt.word2vec'
    model=KeyedVectors.load_word2vec_format(filename,binary=False)
    table = str.maketrans('', '', string.punctuation)
    
    height=100
    width=50
    imdb=imdb_dataset('data/imdb-train.csv',height,width,model)
    
    tau=1
    current=0
    mb_size=80
    total_doc=25000
    channel=1
    loss_ls=[]
    valloss_ls=[]
    train_acc=0
    
    indices=list(range(len(imdb)))
    # np.random.shuffle(indices)
    train_sampler = SubsetRandomSampler(indices[0:20000])
    # train_sampler = SequentialSampler(indices[0:mb_size])
    val_sampler = SubsetRandomSampler(indices[20000:25000])
    # val_sampler = SequentialSampler(indices[20000:250000])
    train_loader=DataLoader(imdb,batch_size=mb_size,num_workers=16,sampler=train_sampler)
    valid_loader=DataLoader(imdb,batch_size=600,num_workers=16,sampler=val_sampler)

    adcnn=ADCNN(1,300,100,5,50)
    if torch.cuda.device_count() > 1:
        adcnn=nn.DataParallel(adcnn)
    adcnn.to(device=device)
    
    optimizer = optim.Adam(adcnn.parameters(),lr=.01)    
    loss_ls=[]
    print("preprocessing done")
#     trainer = IMDBClassifier(500,50)
    if args.train:
        print('Training...')
        itrn=0
        num_itrn=1
        total_loss=0
        trained=torch.load("save_adcnn/"+"only_sent_139")
        adcnn.load_state_dict(trained['model'])
        optimizer.load_state_dict(trained['optimizer'])
        adcnn.train()
        while(itrn<num_itrn):
            train_acc=0
            for numbatch,mb in enumerate(train_loader):
                X=mb['review'].to(device=device)
                word_weight=mb['word_weight'].to(device=device)
                word_weight2=mb['word_weight2'].to(device=device)
                lbl=mb['rating'].to(device=device,dtype=torch.float)
                optimizer.zero_grad()
                ypred,encoded_X,ylabel=adcnn(X.unsqueeze_(1))
                ylabel=ylabel.to(device=device)

                ypred=ypred/torch.norm(ypred,2,3,keepdim=True)
                a=torch.matmul(ypred,imdb.vocabmat)*tau
                b=torch.exp(a)
                temp=torch.sum(b,3)
                c=torch.matmul(X.view(X.shape[0],1,height,1,width),ypred.view(ypred.shape[0],1,height,width,1))*tau
                temp1=torch.exp(c)
                d=torch.log(temp1.view(X.shape[0],1,height)/temp)
                e=word_weight*word_weight2
                loss0=-torch.sum(torch.matmul(d,e.unsqueeze_(2)))

        #         loss=-torch.sum(d)/X.shape[0]*imdb.h
                loss1=-torch.dot(lbl,torch.log(ylabel.reshape(lbl.shape[0])))-torch.dot((1-lbl),torch.log((1-ylabel).reshape(lbl.shape[0])))

                loss=loss1/X.shape[0]
                loss.backward()
                optimizer.step()
                total_loss=total_loss+loss.item()
                ylabel.round_()
                temp=torch.sum(ylabel.data.reshape(lbl.shape)==lbl.data)
                train_acc+=temp.item()
        #         if(numbatch%10==0):
        #             print('batch',numbatch)
        # #             print('batch loss0 ',loss0.item())
        #             print('batch loss1 ',loss1.item())
        #             print('batch loss ',loss.item())

            adcnn.eval()
            with torch.no_grad():
                val_acc=0
                for (num_val,val_dict) in enumerate(valid_loader):
                    tb=val_dict['review'].to(device,dtype=torch.float)
                    tb.unsqueeze_(1)    
                    tlbl=val_dict['rating'].to(device,dtype=torch.float)
                    ex1,ex2,typred=adcnn(tb)
                    val_loss=-torch.dot(tlbl,torch.log2(typred.reshape(tlbl.shape[0])))-torch.dot((1-tlbl),torch.log2((1-typred).reshape(tlbl.shape[0])))
            #       val_loss=criterion(typred.reshape(tlbl.shape[0]),tlbl)
                    #loss=criterion(ypred,lbl)
                    typred.round_()
                    temp1=torch.sum(typred.data.reshape(tlbl.shape)==tlbl.data)
                    val_acc+=temp1.item()

                print("Training loss ",total_loss)
                print("Training Accuracy",float(train_acc)/20000)
                print("Validation loss ",val_loss)
                print('Validation Accuracy',float(val_acc)/5000)
#                 loss_ls.append(total_loss)
#                 valloss_ls.append(val_loss.item())
        #     if(itrn%20==9):
        #         state={'model':adcnn.state_dict(),'optimizer':optimizer.state_dict(),'m_itrn':itrn}
        #         torch.save(state,"save_adcnn/"+"rec_sent_"+str(itrn))
            itrn+=1
            total_loss=0   
        state={'model':adcnn.state_dict(),'optimizer':optimizer.state_dict(),'m_itrn':itrn}
        torch.save(state,"current_imdb")
    if args.test:
        print('Testing...')
        test_out_file = open('result/imdb-test.csv', 'w')
        test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)
        imdbt=imdb_dataset_test('data/imdb-test.csv',height,width,model)
        indices=range(len(imdbt))
        train_sampler = SequentialSampler(indices)
        train_loader=DataLoader(imdbt,batch_size=1,num_workers=8,sampler=train_sampler)
        df=pd.read_csv('data/imdb-test.csv')
        test_writer.writerow(['review', 'rating'])
        
        trained=torch.load("save_adcnn/"+"only_sent_139")
        adcnn.load_state_dict(trained['model'])
        optimizer.load_state_dict(trained['optimizer'])
        
        adcnn.eval()
        count=0
        for (num_batch,mb_dict) in enumerate(train_loader):
                mb=mb_dict['review'].to(device,dtype=torch.float)
                mb.unsqueeze_(1)
#                 lbl=mb_dict['rating'].to(device,dtype=torch.float)
                y,encoded_X,ypred=adcnn(mb)
                ypred.round_()
#                 print(int(ypred.item()))
                prediction = ['neg','pos']
                test_writer.writerow([df.loc[num_batch][0], prediction[int(ypred.item())]])
#         trainer.predict()
    print("end")