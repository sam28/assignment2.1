#!/bin/python3
import csv, random, argparse
import torch
import numpy as np
import pandas as pd
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset,DataLoader
from torch.utils.data import random_split
from gensim.models import KeyedVectors
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.sampler import SequentialSampler
import string

class imdb_dataset(Dataset):
    def __init__(self,path,transform,h,w,embad_model):
        self.data_frame=pd.read_csv(path)
        self.transform=transform
        self.model=embad_model
        self.h=h
        self.w=w
    
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        sample=self.data_frame.loc[idx]
        if self.transform:
            mat,label=self.transform(sample,self.model,self.h,self.w)
        return {'review':mat,'rating':label}
    
class imdb_dataset_test(Dataset):
    def __init__(self,path,transform,h,w,embad_model):
        self.data_frame=pd.read_csv(path)
        self.transform=transform
        self.model=embad_model
        self.h=h
        self.w=w
    
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        sample=self.data_frame.loc[idx]
        if self.transform:
            mat=self.transform(sample,self.model,self.h,self.w)
        return {'review':mat}


def sen_to_mat_test(r,model,h,w):
    X=torch.randn((h,w))
    label=0
    table = str.maketrans('', '', string.punctuation)
    for j,w in enumerate(r[0].split(' ')):
        w1=w.translate(table)
        if w1 in model.vocab:
            X[j,:]=torch.Tensor(model[w1]).view(1,-1)
        if(j>=h-1):
            break
    return X

def sen_to_mat(r,model,h,w):
    X=torch.randn((h,w))
    label=0
    table = str.maketrans('', '', string.punctuation)
    for j,w in enumerate(r[0].split(' ')):
        w1=w.translate(table)
        if w1 in model.vocab:
            X[j,:]=torch.Tensor(model[w1]).view(1,-1)
        if r[1]=='pos':
            label=1
        else:
            label=0
        if(j>=h-1):
            break
    return X,label


class Net(torch.nn.Module):
    def __init__(self,in_c,out_c,kernel):
        super(Net,self).__init__()
        self.conv1=nn.Conv2d(in_c,out_c,(2,kernel+2),padding=1)
        self.conv2=nn.Conv2d(in_c,out_c,(3,kernel+4),padding=2)
        self.conv3=nn.Conv2d(in_c,out_c,(4,kernel+6),padding=3)
        self.conv4=nn.Conv2d(in_c,out_c,(5,kernel+8),padding=4)
        self.fc1=torch.nn.Linear(out_c*4,1)
    def forward(self,x):
        x1=F.relu(self.conv1(x))
        x1=F.max_pool2d(x1,(x1.shape[2], 1), 1)
        x2=F.relu(self.conv2(x))
        x2=F.max_pool2d(x2,(x2.shape[2], 1), 1)
        x3=F.relu(self.conv3(x))
        x3=F.max_pool2d(x3,(x3.shape[2], 1), 1)
        x4=F.relu(self.conv3(x))
        x4=F.max_pool2d(x4,(x4.shape[2], 1), 1)
        cat1=torch.cat((x1,x2),1)
        cat2=torch.cat((x3,x4),1)
        cat2=torch.cat((cat1,cat2),1)
        flat=cat2.view(cat2.shape[0],-1)
        out=torch.sigmoid(self.fc1(F.dropout(flat,p=.3)))
        return out
    
class IMDBClassifier:
    def __init__(self,height,width):
        train_file = open('data/imdb-train.csv', 'r')
        self.train_reader = csv.reader(train_file)

        test_in_file = open('data/imdb-test.csv', 'r')
        self.test_reader = csv.reader(test_in_file)

        # Skip the header row
        next(self.train_reader,None)
        next(self.test_reader,None)
        
        self.table = str.maketrans('', '', string.punctuation)
        self.height=height
        self.width=width
        self.filename='glove.6B.50d.txt.word2vec'
        self.model=KeyedVectors.load_word2vec_format(self.filename,binary=False)
        self.table = str.maketrans('', '', string.punctuation)
        self.cnn_model=Net(1,50,50)
        if torch.cuda.device_count() > 1:
            self.cnn_model=nn.DataParallel(self.cnn_model)
        self.cnn_model.to(device)
        self.imdb=imdb_dataset('data/imdb-train.csv',sen_to_mat,height,width,self.model)
        
    def train(self):
        filename=self.filename
        cnn_model=self.cnn_model
        model=self.model
        table=self.table
        imdb=self.imdb
        
        current=0
        mb_size=300
        total_doc=25000
        channel=1
        height=500
        width=50
        loss_ls=[]
        valloss_ls=[]
        train_acc=0
        save_model_name="imdb_current"
        
#         crt=nn.BCELoss()
#         criterion=crt.to(device)
        optimizer = optim.Adam(cnn_model.parameters(),lr=.001)
        indices=list(range(0,25000))
        np.random.shuffle(indices)
        train_sampler = SubsetRandomSampler(indices[0:20000])
#         train_sampler = SequentialSampler(indices[0:200000])
        val_sampler = SubsetRandomSampler(indices[20000:250000])
        # val_sampler = SequentialSampler(indices[20000:250000])

        train_loader=DataLoader(imdb,batch_size=mb_size,num_workers=8,sampler=train_sampler)
        valid_loder=DataLoader(imdb,batch_size=mb_size,num_workers=8,sampler=val_sampler)
        itrn_loss=[]
        itrn=0
        num_itrn=60
        total_loss=0
        while(itrn<num_itrn):
            print('itrn',itrn)
            train_acc=0
            cnn_model.train()
            for (num_batch,mb_dict) in enumerate(train_loader):
                mb=mb_dict['review'].to(device,dtype=torch.float)
                mb.unsqueeze_(1)
                lbl=mb_dict['rating'].to(device,dtype=torch.float)
                optimizer.zero_grad()
                ypred=cnn_model(mb)
                loss=-torch.dot(lbl,torch.log(ypred.reshape(lbl.shape[0])))-torch.dot((1-lbl),torch.log((1-ypred).reshape(lbl.shape[0])))
                loss.backward()
                optimizer.step()
                total_loss=total_loss+loss.item()
                ypred.round_()
                temp=(ypred.data.reshape(lbl.shape)==lbl.data).sum()
                train_acc+=temp.item()
                if(num_batch*mb_size%2000==0):
                    print(loss.item())
                    itrn_loss.append(loss.item())
            cnn_model.eval()
            with torch.no_grad():
                val_acc=0
                for (num_val,val_dict) in enumerate(valid_loder):
                    tb=val_dict['review'].to(device,dtype=torch.float)
                    tb.unsqueeze_(1)    
                    tlbl=val_dict['rating'].to(device,dtype=torch.float)
                    typred=cnn_model(tb)
                    val_loss=-torch.dot(tlbl,torch.log2(typred.reshape(tlbl.shape[0])))-torch.dot((1-tlbl),torch.log2((1-typred).reshape(tlbl.shape[0])))
            #       val_loss=criterion(typred.reshape(tlbl.shape[0]),tlbl)
                    #loss=criterion(ypred,lbl)
                    typred.round_()
                    temp1=(typred.data.reshape(tlbl.shape)==tlbl.data).sum()
                    val_acc+=temp1.item()

                print("Training loss ",total_loss)
                print("Training Accuracy",float(train_acc)/20000)
                print("Validation loss ",val_loss)
                print('Validation Accuracy',float(val_acc)/5000)
                loss_ls.append(total_loss)
                valloss_ls.append(val_loss.item())
                itrn=itrn+1
                total_loss=0;
        torch.save(cnn_model.state_dict(), save_model_name)
        
        
    def predict(self):
        test_out_file = open('result/imdb-test.csv', 'w')
        self.test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)
        imdb=imdb_dataset_test('data/imdb-test.csv',sen_to_mat_test,self.height,self.width,self.model)
        indices=range(len(imdb))
        train_sampler = SequentialSampler(indices)
        train_loader=DataLoader(imdb,batch_size=1,num_workers=8,sampler=train_sampler)
        df=pd.read_csv('data/imdb-test.csv')
        self.test_writer.writerow(['review', 'rating'])
        self.cnn_model.load_state_dict(torch.load("saved_model/itrn120tacc18396000vacc183000"))
        self.cnn_model.eval()
        count=0
        for (num_batch,mb_dict) in enumerate(train_loader):
                mb=mb_dict['review'].to(device,dtype=torch.float)
                mb.unsqueeze_(1)
#                 lbl=mb_dict['rating'].to(device,dtype=torch.float)
                ypred=self.cnn_model(mb)
                ypred.round_()
#                 print(int(ypred.item()))
                prediction = ['neg','pos']
                self.test_writer.writerow([df.loc[num_batch][0], prediction[int(ypred.item())]])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("in main")
    args = parser.parse_args()

    trainer = IMDBClassifier(500,50)
    if args.train:
        print('Training...')
        trainer.train()
    if args.test:
        print('Testing...')
        trainer.predict()
