#!/bin/python3
import csv, random, argparse
import pandas as pd
import numpy as np
from gensim.models import KeyedVectors
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import string
import pickle

class SNLIClassifier:
    def __init__(self):
        train_file = open('data/snli-train.csv', 'r')
        self.train_reader = csv.reader(train_file)

        valid_file = open('data/snli-dev.csv', 'r')
        self.valid_reader = csv.reader(valid_file)

        test_in_file = open('data/snli-test.csv', 'r')
        self.test_reader = csv.reader(test_in_file)
        
        
        filename='glove.6B.200d.txt.word2vec'
        self.model=KeyedVectors.load_word2vec_format(filename,binary=False)
        # Skip the header row
        next(self.train_reader)
        next(self.valid_reader)
        next(self.test_reader)
        self.table = str.maketrans('', '', string.punctuation)

    def train(self):
        ## Your training logic goes here
        data_frame=pd.read_csv('data/snli-train.csv')
#         data_frame.dropna(subset=['sentence1','sentence2'], how='any', inplace = True)
#         data_frame.reset_index(drop=True,inplace=True)
        model=self.model
        table=self.table
        label=[]
        
        X=np.random.randn(len(data_frame),400)
#         for r in test_reader:
#             test_corpus.append(r[0])
        bool_na=data_frame.isna()
        for r in range(len(data_frame)):
            label.append(data_frame.loc[r][2])
            if bool_na.loc[r][0] or bool_na.loc[r][1] or bool_na.loc[r][2]:
                continue
            sum_v=np.zeros((1,200)).reshape(1,200)
            count=1
            for w in data_frame.loc[r]['sentence1'].split(' '):             
                w1=w.translate(self.table)
                if w1 in model.vocab:
                    sum_v=sum_v+model[w1].reshape(1,200)
                    coun=count+1
                temp1=sum_v/count
            count=1
            sum_v=np.zeros((1,200)).reshape(1,200)
            for w in data_frame.loc[r]['sentence2'].split(' '):
                w1=w.translate(self.table)
                if w1 in model.vocab:
                    sum_v=sum_v+model[w1].reshape(1,200)
                    coun=count+1
                temp2=sum_v/count
            X[r,0:200]=temp1
            X[r,200:]=temp2
            
        test_prcnt=0
        X_train,X_test, y_train, y_test = train_test_split(X,label, test_size=test_prcnt, random_state=0)
        model1=LogisticRegression(n_jobs=2)
        model1.fit(X_train,y_train)
        with open('snli_logistic_current.pkl', 'wb') as f:
            pickle.dump(model1, f)

        pass

    def predict(self):
        test_out_file = open('result/snli-test.csv', 'w')
        self.test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)
        data_frame=pd.read_csv('data/snli-test.csv')
        bool_na=data_frame.isna()
        model=self.model
        table=self.table
        
#         data_frame.dropna(subset=['sentence1','sentence2'], how='any', inplace = True)
#         data_frame.reset_index(drop=True,inplace=True)
        X=np.random.randn(len(data_frame),400)
        for r in range(len(data_frame)):
            sum_v=np.zeros((1,200)).reshape(1,200)
            count=1
            if bool_na.loc[r][0] or bool_na.loc[r][1]:
                continue
            for w in data_frame.loc[r]['sentence1'].split(' '):             
                w1=w.translate(self.table)
                if w1 in model.vocab:
                    sum_v=sum_v+model[w1].reshape(1,200)
                    coun=count+1
                temp1=sum_v/count
            count=1
            sum_v=np.zeros((1,200)).reshape(1,200)
            for w in data_frame.loc[r]['sentence2'].split(' '):
                w1=w.translate(self.table)
                if w1 in model.vocab:
                    sum_v=sum_v+model[w1].reshape(1,200)
                    coun=count+1
                temp2=sum_v/count
            X[r,0:200]=temp1
            X[r,200:]=temp2
        
        with open('snli_logistic.pkl', 'rb') as f:
            clf = pickle.load(f)
            
        pred=clf.predict(X)
        self.test_writer.writerow(['sentence1', 'sentence2', 'label'])
        for i in range(len(data_frame)):
            self.test_writer.writerow([data_frame.loc[i][0],data_frame.loc[i][1],pred[i]])
#         for entry in self.test_reader:
#             ## Your prediction logic goes here
#             prediction = random.choice(['neutral', 'entailment', 'contradiction'])
#             self.test_writer.writerow([entry[0], entry[1], prediction[]])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
    args = parser.parse_args()

    trainer = SNLIClassifier()
#     trainer.train()
    
    if args.train:
        print('Training...')
        trainer.train()

    if args.test:
        print('Testing...')
        trainer.predict()
